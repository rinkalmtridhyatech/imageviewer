import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:image_viewer/views/splash_view.dart';

import '../style/color_constants.dart';
import '../utils/app_constants.dart';

class MyApp extends StatefulWidget {
  @override
  MyAppState createState() => MyAppState();
}

class MyAppState extends State<MyApp> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primarySwatch: ColorConstants.appColor,
      ),
      builder: (BuildContext context, Widget? child) {
        final MediaQueryData data = MediaQuery.of(context);
        return MediaQuery(
            child: child!, data: data.copyWith(textScaleFactor: 1));
      },
      initialRoute: "/",
      home: SplashView(),
      title: AppConstants.appName,
    );
  }
}
