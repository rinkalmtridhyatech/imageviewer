import 'package:cached_network_image/cached_network_image.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:image_viewer/utils/image_paths.dart';
import 'package:photo_view/photo_view.dart';

import '../style/dimensions.dart';

class ImageViewer extends StatefulWidget {
  final List<String> imageGallery;

  const ImageViewer({
    Key? key,
    required this.imageGallery,
  }) : super(key: key);

  @override
  ImageViewerState createState() => ImageViewerState();
}

class ImageViewerState extends State<ImageViewer> {
  final CarouselController _controller = CarouselController();
  var image = "";
  var currentIndex = 0;

  @override
  Widget build(BuildContext context) {
    Dimensions.screenWidth = MediaQuery.of(context).size.width;
    Dimensions.screenHeight = MediaQuery.of(context).size.height;
    return Scaffold(
      body: SingleChildScrollView(
          child: widget.imageGallery.length == 1
              ? SizedBox(
                  height: Dimensions.screenHeight,
                  width: Dimensions.screenWidth,
                  child: Padding(
                    padding: const EdgeInsets.symmetric(vertical: 28.0),
                    child: Center(
                      child: PhotoView(
                        imageProvider: CachedNetworkImageProvider(
                          widget.imageGallery.first,
                        ),
                      ),
                    ),
                  ),
                )
              : SizedBox(
                  height: Dimensions.screenHeight,
                  width: Dimensions.screenWidth,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Stack(
                        children: [
                          CachedNetworkImage(
                            height: Dimensions.screenHeight / 3,
                            imageUrl: widget.imageGallery[currentIndex],
                            imageBuilder: (context, imageProvider) => Container(
                              decoration: BoxDecoration(
                                  image: DecorationImage(
                                image: imageProvider,
                                fit: BoxFit.cover,
                              )),
                            ),
                            placeholder: (context, url) => Image.asset(
                              ImagePath.placeHolder,
                              fit: BoxFit.cover,
                            ),
                            errorWidget: (context, url, error) =>
                                const Icon(Icons.error),
                          ),
                          SizedBox(
                            height: Dimensions.screenHeight / 3,
                            child: Row(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                button(Icons.arrow_back_ios, () {
                                  _controller.previousPage();
                                }),
                                button(Icons.arrow_forward_ios, () {
                                  _controller.nextPage();
                                }),
                              ],
                            ),
                          ),
                        ],
                      ),
                      CarouselSlider(
                        items: widget.imageGallery
                            .map((item) => InkWell(
                                  onTap: () {
                                    setState(() {
                                      image = item;
                                    });
                                  },
                                  child: Container(
                                    margin: const EdgeInsets.only(top: 18.0),
                                    child: CachedNetworkImage(
                                      width: 160,
                                      imageUrl: item,
                                      fit: BoxFit.cover,
                                      placeholder: (context, url) =>
                                          Image.asset(
                                        ImagePath.placeHolder,
                                        fit: BoxFit.cover,
                                      ),
                                      errorWidget: (context, url, error) =>
                                          const Icon(Icons.error),
                                    ),
                                  ),
                                ))
                            .toList(),
                        carouselController: _controller,
                        options: CarouselOptions(
                          viewportFraction: 0.5,
                          onPageChanged: (index, reason) {
                            setState(() {
                              currentIndex = index;
                            });
                          },
                          height: 160,
                        ),
                      ),
                    ],
                  ))),
    );
  }

  button(
    IconData? icon,
    VoidCallback? callback,
  ) {
    return InkWell(
      onTap: callback,
      child: Container(
        padding: const EdgeInsets.all(10),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(8),
          color: const Color(0xC8000000),
          border: null,
        ),
        child: Icon(
          icon,
          size: 26.0,
          color: Colors.white,
        ),
      ),
    );
  }
}
