import 'package:flutter/material.dart';
import 'package:image_viewer/style/color_constants.dart';
import 'package:image_viewer/utils/image_paths.dart';
import 'package:image_viewer/views/image_viewer.dart';

import '../style/dimensions.dart';

class SplashView extends StatelessWidget {
  SplashView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Dimensions.screenWidth = MediaQuery.of(context).size.width;
    Dimensions.screenHeight = MediaQuery.of(context).size.height;
    return Scaffold(
      body: Container(
        color: Colors.white,
        height: Dimensions.screenHeight,
        width: Dimensions.screenWidth,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            const Text(
              "Image Viewer",
              style: TextStyle(
                fontWeight: FontWeight.w700,
                fontSize: 35,
                color: Colors.black,
              ),
              textAlign: TextAlign.center,
            ),
            const SizedBox(
              height: 45,
            ),
            InkWell(
              onTap: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => const ImageViewer(
                              imageGallery: [ImagePath.image1],
                            )));
              },
              child: Container(
                height: 40,
                padding: const EdgeInsets.all(8),
                decoration: BoxDecoration(
                  color: ColorConstants.pink,
                  borderRadius: BorderRadius.circular(4),
                ),
                child: const Text(
                  "Add Single Image",
                  style: TextStyle(
                    fontSize: 20,
                    color: Colors.white,
                  ),
                  textAlign: TextAlign.center,
                ),
              ),
            ),
            const SizedBox(
              height: 30,
            ),
            InkWell(
              onTap: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => const ImageViewer(
                              imageGallery: [
                                ImagePath.image1,
                                ImagePath.image2,
                                ImagePath.image3,
                                ImagePath.image4,
                                ImagePath.image5,
                                ImagePath.image6
                              ],
                            )));
              },
              child: Container(
                height: 40,
                padding: const EdgeInsets.all(8),
                decoration: BoxDecoration(
                  color: ColorConstants.pink,
                  borderRadius: BorderRadius.circular(4),
                ),
                child: const Text(
                  "Add Multiple Image",
                  style: TextStyle(
                    fontSize: 20,
                    color: Colors.white,
                  ),
                  textAlign: TextAlign.center,
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
