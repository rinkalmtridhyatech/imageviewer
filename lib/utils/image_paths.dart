class ImagePath {
  static const String icSplash = 'assets/images/ic_splash.png';
  static const String placeHolder = 'assets/images/placeholder_image.png';
  static const String image1 =
      'https://burst.shopifycdn.com/photos/hiker-looks-out-over-bay-surrounded-by-mountains.jpg?width=925&format=pjpg&exif=1&iptc=1';

  static const String image2 =
      'https://burst.shopifycdn.com/photos/prairie-woman-at-sunset.jpg?width=925&format=pjpg&exif=1&iptc=1';
  static const String image3 =
      'https://burst.shopifycdn.com/photos/woman-walks-bridge.jpg?width=925&format=pjpg&exif=1&iptc=1';
  static const String image4 =
      'https://burst.shopifycdn.com/photos/turquoise-blue-ocean-waters-along-jungle-covered-cliffside.jpg?width=925&format=pjpg&exif=1&iptc=1';

  static const String image5 =
      'https://burst.shopifycdn.com/photos/golden-autumn-leaves.jpg?width=925&format=pjpg&exif=1&iptc=1';

  static const String image6 =
      'https://burst.shopifycdn.com/photos/drone-view-of-car-driving-through-forest.jpg?width=925&format=pjpg&exif=1&iptc=1';
}
